import { MigrationInterface, QueryRunner } from "typeorm";

export class String1703073275610 implements MigrationInterface {
    name = 'String1703073275610'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "pago" DROP COLUMN "carritoId"`);
        await queryRunner.query(`ALTER TABLE "pago" ADD "carritoId" character varying NOT NULL`);
        await queryRunner.query(`ALTER TABLE "pago" DROP COLUMN "usuarioId"`);
        await queryRunner.query(`ALTER TABLE "pago" ADD "usuarioId" character varying NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "pago" DROP COLUMN "usuarioId"`);
        await queryRunner.query(`ALTER TABLE "pago" ADD "usuarioId" integer NOT NULL`);
        await queryRunner.query(`ALTER TABLE "pago" DROP COLUMN "carritoId"`);
        await queryRunner.query(`ALTER TABLE "pago" ADD "carritoId" integer NOT NULL`);
    }

}
