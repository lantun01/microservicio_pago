import { MigrationInterface, QueryRunner } from "typeorm";

export class Initial1703051148459 implements MigrationInterface {
    name = 'Initial1703051148459'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "pago" ("id" SERIAL NOT NULL, "carritoId" integer NOT NULL, "usuarioId" integer NOT NULL, "total" integer NOT NULL, "urlRetorno" character varying NOT NULL DEFAULT 'http://tbk-node-test.continuumhq.dev/webpay_plus/commit', "visible" boolean NOT NULL, "token" character varying NOT NULL, "url" character varying NOT NULL, "PagoAt" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "PK_6be14be998d5e41f10e58c0e651" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "pago"`);
    }

}
