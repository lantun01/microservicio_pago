import { MigrationInterface, QueryRunner } from "typeorm";

export class Visible1703073543293 implements MigrationInterface {
    name = 'Visible1703073543293'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "pago" ALTER COLUMN "visible" DROP NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "pago" ALTER COLUMN "visible" SET NOT NULL`);
    }

}
