import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, Timestamp } from "typeorm";

@Entity({name:'pago'})
export class Pago {
    @PrimaryGeneratedColumn()
    id:number;

    @Column()
    carritoId:string;

    @Column()
    usuarioId:string;

    @Column()
    total:number;

    @Column({default:'http://tbk-node-test.continuumhq.dev/webpay_plus/commit'})
    urlRetorno:string;

    @Column({nullable:true})
    visible:boolean;

    @Column()
    token:string;

    @Column()
    url:string;

    @CreateDateColumn()
    PagoAt:Timestamp;
}
