import { Column } from "typeorm";

export class CreatePagoDto {

    @Column()
    carritoId:string;

    @Column()
    usuarioId:string;

    @Column()
    total:number;

    @Column()
    urlRetorno:string;
}
