import { Injectable, NotFoundException } from '@nestjs/common';
import { CreatePagoDto } from './dto/create-pago.dto';
import { UpdatePagoDto } from './dto/update-pago.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Pago } from './entities/pago.entity';
import { Repository } from 'typeorm';

@Injectable()
export class PagoService {
  constructor(@InjectRepository(Pago)private readonly PagoRepository:Repository<Pago>){}


  async create(createPagoDto: CreatePagoDto) {
    const WebpayPlus = require('transbank-sdk').WebpayPlus;
    const pago= this.PagoRepository.create(createPagoDto);
    const createResponse = await (new WebpayPlus.Transaction()).create(
      pago.carritoId,
      pago.usuarioId,
      pago.total,
      pago.urlRetorno
    )
    console.log(createResponse)
    pago.token=createResponse.token;
    pago.url=createResponse.url
    this.PagoRepository.save(pago);
    return createResponse;
  }

  async findOne(id: number) {
    const pago= await this.PagoRepository.findOne({
      where: {id:id}
    });

    if(!pago) throw new NotFoundException('pago no encontrado');
    return pago;
  }

  async update(UpdatePagoDto: Partial<UpdatePagoDto>):Promise<Pago> {
    const new_pago =await this.findOne(UpdatePagoDto.id);
    Object.assign(new_pago,UpdatePagoDto)
    return await this.PagoRepository.save(new_pago);
  }
}
