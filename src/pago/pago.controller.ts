import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { PagoService } from './pago.service';
import { CreatePagoDto } from './dto/create-pago.dto';
import { UpdatePagoDto } from './dto/update-pago.dto';

@Controller()
export class PagoController {
  constructor(private readonly pagoService: PagoService) {}

  @MessagePattern('createPago')
  create(@Payload() createPagoDto: CreatePagoDto) {
    return this.pagoService.create(createPagoDto);
  }

  @MessagePattern('findOnePago')
  findOne(@Payload() id: number) {
    return this.pagoService.findOne(id);
  }

  @MessagePattern('updatePago')
  update(@Payload() updatePagoDto: UpdatePagoDto) {
    return this.pagoService.update(updatePagoDto);
  }
}
